import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastController } from '@ionic/angular';
import { Data } from '../interfaces/data.interface';
@Injectable({
  providedIn: 'root',
})
export class ApiService {
  public arr = [];
  constructor(
    public http: HttpClient,
    public toastController: ToastController
  ) {}

  public searchData(query: string): Promise<any> {
    try {
      return this.http
        .get('https://api.mercadolibre.com/sites/MLA/search?q=:' + query)
        .toPromise()
        .then((response) => response['results'])
        .catch((err) => console.error(err));
    } catch (error) {
      console.error(error);
    }
  }
  public detailData(id: string): Observable<Data[]> {
    try {
      return this.http.get<Data[]>('https://api.mercadolibre.com/items/' + id);
    } catch (error) {
      console.log(error);
    }
  }
  public descriptionData(id: string): Observable<any> {
    try {
      return this.http.get(
        'https://api.mercadolibre.com/items/' + id + '/description'
      );
    } catch (error) {
      console.log(error);
    }
  }
  public addFav(data: any) {
    try {
      this.arr.push(data);
      if (this.arr) {
        this.sendMessage('Product was add to favorites');
      } else {
        this.sendMessage("Product wasn't add to favorites");
      }
    } catch (error) {}
  }
  public seeData() {
    return this.arr;
  }
  async sendMessage(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 3000,
      mode: 'ios',
    });
    toast.present();
  }
}
