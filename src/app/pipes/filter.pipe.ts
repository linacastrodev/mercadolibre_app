import { Pipe, PipeTransform } from '@angular/core';
import { Data } from '@angular/router';

@Pipe({
  name: 'filter',
})
export class FilterPipe implements PipeTransform {
  transform(arr: any[], text: string): any[] {
    if (!arr) return [];
    if (!text) return arr;
    text = text.toLowerCase();
    return arr.filter((it) => {
      return it.title.toLowerCase().includes(text);
    });
  }
}
