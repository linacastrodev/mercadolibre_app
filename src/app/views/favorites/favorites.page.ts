import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.page.html',
  styleUrls: ['./favorites.page.scss'],
})
export class FavoritesPage implements OnInit {
  public dataFav: any;
  constructor(public apiService: ApiService) {}

  ngOnInit() {
    this.seeData();
  }
  seeData() {
    this.dataFav = this.apiService.seeData();
  }
}
