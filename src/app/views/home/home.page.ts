import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  public data: any;
  public allData: any;
  public textSearch = '';
  public show: boolean = true;
  constructor(public dataService: ApiService, public router: Router) {}

  ngOnInit() {}
  async search($event) {
    this.show = false;
    this.textSearch = $event.detail.value;
    await this.dataService.searchData(this.textSearch).then((response) => {
      this.data = response;
      console.log(this.data);
      return this.data;
    });
  }
  async goDetail(items: any) {
    try {
      await this.router.navigate(['tabs/detail', items.id]);
    } catch (error) {
      console.error(error);
    }
  }
}