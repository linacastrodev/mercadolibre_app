import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  public id: any;
  public data = {};
  public description = {};
  constructor(
    public apiService: ApiService,
    private route: ActivatedRoute,
    public router: Router
  ) {}

  ngOnInit() {
    this.route.params.subscribe((data: any) => {
      this.id = data;
    });
    this.getData();
    this.descriptionData();
  }
  async getData() {
    try {
      await this.apiService.detailData(this.id.id).subscribe((response) => {
        this.data = response;
        console.log(this.data);
      });
    } catch (error) {
      console.error(error);
    }
  }
  async descriptionData() {
    try {
      await this.apiService
        .descriptionData(this.id.id)
        .subscribe((response) => {
          this.description = response;
          console.log(this.description);
        });
    } catch (error) {
      console.error(error);
    }
  }
  async goFav(data: any) {
    try {
      await this.apiService.addFav(data);
      this.router.navigateByUrl('tabs/fav');
    } catch (error) {
      console.error(error);
    }
  }
}