export interface Data{
    id:string;
    thumbnail:string;
    title:string;
    price:string;
    currency_id:string;
    condition:string;
    available_quantity:number;
    accepts_mercadopago:boolean;
}